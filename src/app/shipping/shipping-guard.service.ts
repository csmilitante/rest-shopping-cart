import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { JwtService } from '../core/services/jwt.service';

@Injectable()
export class ShippingGuardService implements CanActivate {
    constructor(private jwtService: JwtService,
                private router: Router) {
    }

    canActivate() {
        if (this.jwtService.getAccessToken() === null ||
            this.jwtService.getJwt() === null
        ) {
            this.router.navigate(['/login-register']);
            return false;
        }

        return true;
    }
}
