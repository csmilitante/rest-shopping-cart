import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../config/environment';

@Injectable()
export class ShippingService {
    constructor(private http: HttpClient) {
    }

    getShippingRates(jwt: string): Observable<any> {
        return this.http.get(`${environment.api.uri}/${environment.api.endpoint.shipping}/${jwt}`);
    }

    saveShippingDetails(data: Object): Observable<any> {
        return this.http.post(`${environment.api.uri}/${environment.api.endpoint.shipping}`, data);
    }
}
