import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { ShippingRoutingModule } from './shipping-routing.module';
import { ShippingDetailsComponent } from './components/shipping-details/shipping-details.component';
import { ShippingService } from './services/shipping.service';
import { ShippingGuardService } from './shipping-guard.service';

@NgModule({
    imports: [
        CommonModule,
        ReactiveFormsModule,
        ShippingRoutingModule
    ],
    declarations: [ShippingDetailsComponent],
    providers: [ShippingService, ShippingGuardService]
})
export class ShippingModule {
}
