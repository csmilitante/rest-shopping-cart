import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ShippingService } from '../../services/shipping.service';
import { JwtService } from '../../../core/services/jwt.service';
import { Router } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http';
import { ErrorHandlerService } from '../../../core/services/error-handler.service';
import { Shipping } from '../../../shared/shipping-model';

@Component({
    selector: 'app-shipping-details',
    templateUrl: './shipping-details.component.html',
    styleUrls: ['./shipping-details.component.css']
})
export class ShippingDetailsComponent implements OnInit {

    shippingForm: FormGroup;
    shippingMethods: Shipping[];

    constructor(private shippingService: ShippingService,
                private jwtService: JwtService,
                private errorHandlerService: ErrorHandlerService,
                private router: Router,
                private formBuilder: FormBuilder) {
    }

    createShippingForm() {
        this.shippingForm = this.formBuilder.group({
            shipping_name: [
                '',
                [
                    Validators.required,
                    Validators.minLength(2),
                    Validators.maxLength(35)
                ]
            ],
            shipping_address1: ['', Validators.required],
            shipping_address2: '',
            shipping_address3: '',
            shipping_city: ['', Validators.required],
            shipping_state: ['', Validators.required],
            shipping_country: ['', Validators.required],
            shipping_mehod: ['', Validators.required],
        });
    }

    ngOnInit() {
        // Create shipping form
        this.createShippingForm();

        // Get shipping rates
        this.shippingService.getShippingRates(this.jwtService.getJwt()).subscribe(
            response => {
                this.shippingMethods = response.shipping.map(method => new Shipping(method));
            }, (err: HttpErrorResponse) => {
                // Set data for ErrorHandler
                this.errorHandlerService.setErrorContainer(err);

                // Invalidate JWT
                this.jwtService.invalidateJwt();

                this.router.navigate(['/']);
            }
        );
    }

    onSubmitShippingForm() {
        if (!this.shippingForm.valid) {
            alert('Form is not valid');
            return false;
        }

        const data = this.shippingForm.value;
        data.jwt = this.jwtService.getJwt();

        this.shippingService.saveShippingDetails(data).subscribe(
            response => {
                this.jwtService.setJwt(response.jwt);
                this.router.navigate(['orders/payment']);
            }, (err: HttpErrorResponse) => {
                // Set data for ErrorHandler
                this.errorHandlerService.setErrorContainer(err);

                this.router.navigate(['/']);
            }
        );
    }

    get shipping_name() {
        return this.shippingForm.get('shipping_name');
    }

    get shipping_address1() {
        return this.shippingForm.get('shipping_address1');
    }

    get shipping_address2() {
        return this.shippingForm.get('shipping_address2');
    }

    get shipping_address3() {
        return this.shippingForm.get('shipping_address3');
    }

    get shipping_city() {
        return this.shippingForm.get('shipping_city');
    }

    get shipping_state() {
        return this.shippingForm.get('shipping_state');
    }

    get shipping_country() {
        return this.shippingForm.get('shipping_country');
    }

    get shipping_mehod() {
        return this.shippingForm.get('shipping_mehod');
    }
}
