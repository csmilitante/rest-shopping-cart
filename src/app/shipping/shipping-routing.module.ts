import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ShippingDetailsComponent } from './components/shipping-details/shipping-details.component';
import { ShippingGuardService } from './shipping-guard.service';

const routes: Routes = [
    {
        path: '',
        component: ShippingDetailsComponent,
        canActivate: [ShippingGuardService]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ShippingRoutingModule {
}
