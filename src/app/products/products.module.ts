import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ProductListComponent } from './components/product-list/product-list.component';
import { ProductsService } from './services/products.service';
import { ProductDetailsComponent } from './components/product-details/product-details.component';
import { ProductsRoutingModule } from './products-routing.module';
import { CartsService } from '../carts/services/carts.service';
import { TransferHttpCacheModule } from '@nguniversal/common';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ProductsRoutingModule,
        TransferHttpCacheModule // https://github.com/angular/universal/tree/master/modules/common
    ],
    declarations: [
        ProductListComponent,
        ProductDetailsComponent
    ],
    providers: [
        CartsService,
        ProductsService
    ]
})
export class ProductsModule {
}
