import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { ProductsService } from '../../services/products.service';
import { NgForm } from '@angular/forms';
import { HttpErrorResponse } from '@angular/common/http';
import { JwtService } from '../../../core/services/jwt.service';
import { CartsService } from '../../../carts/services/carts.service';
import { ErrorHandlerService } from '../../../core/services/error-handler.service';
import { Product } from '../../../shared/product-model';
import { CartItem } from '../../../shared/cart-item-model';

@Component({
    selector: 'app-product-details',
    templateUrl: './product-details.component.html',
    styleUrls: ['./product-details.component.css']
})
export class ProductDetailsComponent implements OnInit {
    product: Product;
    qtyValue: number = 1;
    cartItems: CartItem[] = [];
    canPurchase = true;


    constructor(private activatedRoute: ActivatedRoute,
                private productService: ProductsService,
                private cartsService: CartsService,
                private jwtService: JwtService,
                private errorHandlerService: ErrorHandlerService,
                private router: Router) {
    }

    ngOnInit() {
        this.activatedRoute.paramMap.subscribe(
            (params) => {
                this.productService.getProductById(parseInt(params.get('id'))).subscribe(
                    data => {
                        this.product = new Product(data['details']);
                        // this.productStock = parseInt(this.product.stock_qty);
                    }, (err: HttpErrorResponse) => {
                        // Set data for ErrorHandler
                        this.errorHandlerService.setErrorContainer(err);

                        this.router.navigate(['/']);
                    }
                );
            }, (err: HttpErrorResponse) => {
                // Set data for ErrorHandler
                this.errorHandlerService.setErrorContainer(err);

                this.router.navigate(['/']);
            }
        );

        // get cart details
        if (this.jwtService.getJwt() !== null) {
            this.cartsService.getCartItems(this.jwtService.getJwt()).subscribe(
                response => {
                    this.cartItems = response.carts.cart_items.map(item => new CartItem(item));

                    if (this.isOrderQtyValid() === false) {
                        this.canPurchase = false;
                    }
                }, (err: HttpErrorResponse) => {
                    this.jwtService.invalidateJwt();
                }
            )
        }
    }

    onSubmit(productOrderForm: NgForm) {

        // Validate qty again
        if (this.isOrderQtyValid() === false) {
            alert('You have reached the maximum order limit for this item. Please select a another item.');
            return false;
        }

        const data = productOrderForm.value;

        if (this.jwtService.getJwt() !== null) {
            data.jwt = this.jwtService.getJwt();
        }

        // send data to server
        this.cartsService.addToCart(data).subscribe(
            response => {
                this.jwtService.setJwt(response.jwt);

                // redirect to cart
                this.router.navigate(['cart']);
            },
            (err: HttpErrorResponse) => {
                // delete data.jwt
                delete data.jwt;

                // clear session storage
                this.jwtService.invalidateAllTokens();

                // Set data for ErrorHandler
                this.errorHandlerService.setErrorContainer(err);
            }
        );
    }

    onValidateQty() {
        if (this.qtyValue <= 0 || this.qtyValue > this.product.stock_qty) {
            this.qtyValue = 1;
        }

        // Validate stock and quantity in cart
        if (this.isOrderQtyValid() === false) {
            this.qtyValue = 1;
        }
    }

    isOrderQtyValid(): boolean {
        let isValid = true;

        if (this.cartItems.length <= 0) {
            return isValid;
        }

        this.cartItems.map(
            (cols: CartItem) => {
                if (this.product !== undefined &&
                    (cols.product_id == this.product.product_id)
                ) {
                    const totalOrderQty = cols.qty + this.qtyValue;

                    if (totalOrderQty > this.product.stock_qty) {
                        isValid = false;
                    }
                }
            }
        );

        return isValid;
    }

    hasStock(productQty: number): boolean {
        return productQty <= 0;
    }
}
