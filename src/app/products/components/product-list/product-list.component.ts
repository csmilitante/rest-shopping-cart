import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { ProductsService } from '../../services/products.service';
import { Product } from '../../../shared/product-model';
import { TransferState, makeStateKey } from '@angular/platform-browser';

const PRODUCTS_KEY = makeStateKey('products');

@Component({
    selector: 'app-product-list',
    templateUrl: './product-list.component.html',
    styleUrls: ['./product-list.component.less']
})
export class ProductListComponent implements OnInit {

    public products: Product[];

    constructor(private productsService: ProductsService,
                private router: Router,
                private state: TransferState) {}

    ngOnInit() {
        this.products = this.state.get(PRODUCTS_KEY, null as any);

        if (!this.products) {
            this.productsService
                .getAllProducts()
                .subscribe(data => {
                        this.products = data['products'].map(res => new Product(res));
                        this.state.set(PRODUCTS_KEY, data['products'].map(res => new Product(res)) as any);
                    }
                );
        }
    }

    onGetProductDetails(productId: number) {
        this.router.navigate(['products', productId]);
    }

    hasStock(productQty: number): boolean {
        return productQty <= 0;
    }
}
