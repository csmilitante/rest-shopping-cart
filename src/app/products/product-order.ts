export class ProductOrder {

    constructor(public id: number,
                public quantity: number) {
    }

}
