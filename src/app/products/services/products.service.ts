import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../config/environment';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class ProductsService {

    constructor(private http: HttpClient) {}

    getAllProducts(): Observable<any> {
        return this.http.get(`${environment.api.uri}/${environment.api.endpoint.products}`);
    }

    getProductById(id: number): Observable<any> {
        return this.http.get(`${environment.api.uri}/${environment.api.endpoint.products}/${id}`);
    }
}
