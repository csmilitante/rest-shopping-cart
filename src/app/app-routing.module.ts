import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppCustomPreloader } from './app-routing-loader';
import { ProductsModule } from './products/products.module';

const appRoutes: Routes = [
    {path: '', redirectTo: '', pathMatch: 'full'},
    {
        path: 'cart',
        loadChildren: 'app/carts/carts.module#CartsModule',
        data: {
            preload: true
        }
    },
    {path: 'login-register', loadChildren: 'app/customers/customers.module#CustomersModule'},
    {path: 'shipping', loadChildren: 'app/shipping/shipping.module#ShippingModule'},
    {path: 'orders', loadChildren: 'app/orders/orders.module#OrdersModule'},
    {path: 'success', loadChildren: 'app/success/success.module#SuccessModule'},
    {path: '**', redirectTo: '/'}
];

@NgModule({
    imports: [
        ProductsModule,
        RouterModule.forRoot(appRoutes,
            {
                preloadingStrategy: AppCustomPreloader
            }
        )
    ],
    exports: [RouterModule],
    providers: [AppCustomPreloader]
})
export class AppRoutingModule {
}
