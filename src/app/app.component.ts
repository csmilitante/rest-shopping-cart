import { Component, DoCheck } from '@angular/core';
import { ErrorHandlerService } from './core/services/error-handler.service';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.less']
})
export class AppComponent implements DoCheck {
    errorContainer: HttpErrorResponse;
    hasError: boolean = false;

    constructor(private errorHandlerService: ErrorHandlerService) {
    }

    ngDoCheck(): void {
        this.errorContainer = this.errorHandlerService.getErrorContainer();

        if (this.errorContainer !== undefined) {
            this.hasError = true;
        }
    }

    onCloseErrorAlert() {
        this.hasError = false;
        this.errorHandlerService.resetErrorContainer();
    }
}
