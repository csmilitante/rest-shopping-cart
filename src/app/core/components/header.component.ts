import { Component, DoCheck, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { JwtService } from '../services/jwt.service';

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.less']
})
export class HeaderComponent implements OnInit, DoCheck {

    isLoggedIn: boolean = false;
    displayName: any = '';

    constructor(private authService: AuthService,
                private jwtService: JwtService) {
    }

    ngOnInit() {
        this.displayCustomerName();
    }

    ngDoCheck() {
        this.displayCustomerName();
    }

    onLogout() {
        this.isLoggedIn = false;
        return this.authService.logout();
    }

    displayCustomerName() {
        this.isLoggedIn = this.authService.isLoggedId();
        if (this.isLoggedIn) {
            this.displayName = this.jwtService.getCustomerName();
        }
    }
}
