import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { JwtService } from './jwt.service';
import { Router } from '@angular/router';
import { environment } from '../../../config/environment';

@Injectable()
export class AuthService {

    constructor(private http: HttpClient,
                private jwtService: JwtService,
                private router: Router) {
    }

    login(data): Observable<any> {
        return this.http.post(`${environment.api.uri}/${environment.api.endpoint.customers}`, data);
    }

    logout() {
        this.jwtService.invalidateAuthToken();
        this.jwtService.invalidateJwt();
        this.jwtService.invalidateCustomerName();

        return this.router.navigate(['/']);
    }

    isLoggedId() {
        return this.jwtService.getAccessToken() !== null
    }
}
