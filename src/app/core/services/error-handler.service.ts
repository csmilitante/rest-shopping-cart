import { Injectable } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';

@Injectable()
export class ErrorHandlerService {
    errorContainer: HttpErrorResponse;

    setErrorContainer(data: HttpErrorResponse): void {
        this.errorContainer = data;
    }

    getErrorContainer(): HttpErrorResponse {
        return this.errorContainer;
    }

    resetErrorContainer(): void {
        delete this.errorContainer;
    }
}
