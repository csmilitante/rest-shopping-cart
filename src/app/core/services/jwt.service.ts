import { Inject, Injectable, PLATFORM_ID } from '@angular/core';
import { isPlatformBrowser } from '@angular/common';

@Injectable()
export class JwtService {

    isBrowser;

    constructor(@Inject(PLATFORM_ID) private platformId) {
        this.isBrowser = isPlatformBrowser(platformId);
    }

    getAccessToken(): string {
        if (this.isBrowser) {
            return window.localStorage.getItem('access_token');
        }

        return null;
    }

    setAccessToken(val: string): void {
        window.localStorage.setItem('access_token', val);
    }

    invalidateAuthToken(): void {
        window.localStorage.removeItem('access_token');
    }

    getJwt(): string {
        if (this.isBrowser) {
            return window.localStorage.getItem('jwt');
        }

        return null;
    }

    setJwt(val: string): void {
        window.localStorage.setItem('jwt', val);
    }

    invalidateJwt(): void {
        window.localStorage.removeItem('jwt');
    }

    setCustomerName(val): void {
        window.localStorage.setItem('customer_name', val);
    }

    getCustomerName(): string {
        if (this.isBrowser) {
            return window.localStorage.getItem('customer_name');
        }

        return null;
    }

    invalidateCustomerName(): void {
        window.localStorage.removeItem('customer_name');
    }

    invalidateAllTokens(): void {
        window.localStorage.removeItem('access_token');
        window.localStorage.removeItem('jwt');
        window.localStorage.removeItem('customer_name');
    }
}
