import { NgModule, Optional, PLATFORM_ID, SkipSelf } from '@angular/core';
import { HeaderComponent } from './components/header.component';
import { CommonModule, isPlatformBrowser } from '@angular/common';
import { ErrorHandlerService } from './services/error-handler.service';
import { AuthGuardService } from './guards/auth-guard.service';
import { AuthService } from './services/auth.service';
import { JwtService } from './services/jwt.service';
import { RouterModule } from '@angular/router';
import { JwtModule } from '@auth0/angular-jwt';
import { HttpClientModule } from '@angular/common/http';

export function tokenGetterFactory() {
    if (typeof window !== 'undefined') {
        return window.localStorage.getItem('access_token');
    }
}

@NgModule({
    imports: [
        CommonModule,
        RouterModule,
        HttpClientModule,
        JwtModule.forRoot({
            config: {
                tokenGetter: tokenGetterFactory,
                whitelistedDomains: ['api.exam.com', 'localhost:8181']
            }
        }),
    ],
    declarations: [
        HeaderComponent
    ],
    exports: [
        HeaderComponent
    ],
    providers: [
        JwtService,
        AuthService,
        AuthGuardService,
        ErrorHandlerService
    ]
})
export class CoreModule {
    constructor(@Optional() @SkipSelf() parentModule: CoreModule) {
        if (parentModule) {
            throw new Error(
                'CoreModule is already loaded. Import it in the AppModule only'
            );
        }
    }
}
