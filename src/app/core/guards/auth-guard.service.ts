import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { JwtService } from '../services/jwt.service';

@Injectable()
export class AuthGuardService implements CanActivate {
    constructor(private jwtService: JwtService,
                private router: Router) {
    }

    canActivate() {
        if (this.jwtService.getAccessToken() === null) {
            this.router.navigate(['/']);
            return false;
        }

        return true;
    }
}
