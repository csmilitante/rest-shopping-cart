import { NgModule } from '@angular/core';
import { LoginRegistrationComponent } from './components/login-registration/login-registration.component';
import { RouterModule, Routes } from '@angular/router';

const customerRoutes: Routes = [
    {
        path: '',
        component: LoginRegistrationComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(customerRoutes)],
    exports: [RouterModule]
})
export class CustomersRoutingModule {
}
