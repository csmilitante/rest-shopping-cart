import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LoginRegistrationComponent } from './components/login-registration/login-registration.component';
import { CustomerService } from './services/customer.service';
import { CustomersRoutingModule } from './customers-routing.module';

@NgModule({
    imports: [
        CommonModule,
        ReactiveFormsModule,
        CustomersRoutingModule
    ],
    declarations: [LoginRegistrationComponent],
    providers: [CustomerService]
})
export class CustomersModule {
}
