import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../config/environment';

@Injectable()
export class CustomerService {

    constructor(private http: HttpClient) {
    }

    register($data): Observable<any> {
        return this.http.post(`${environment.api.uri}/${environment.api.endpoint.customers}`, $data);
    }
}
