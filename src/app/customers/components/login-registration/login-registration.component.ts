import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CustomerService } from '../../services/customer.service';
import { JwtService } from '../../../core/services/jwt.service';
import { HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';
import { AuthService } from '../../../core/services/auth.service';
import { ErrorHandlerService } from '../../../core/services/error-handler.service';
import { confirmPasswordValidator } from '../../confirm-password.directive';

@Component({
    selector: 'app-login-registration',
    templateUrl: './login-registration.component.html',
    styleUrls: ['./login-registration.component.css']
})
export class LoginRegistrationComponent implements OnInit {

    loginForm: FormGroup;
    registrationForm: FormGroup;

    constructor(private customerService: CustomerService,
                private jwtService: JwtService,
                private authService: AuthService,
                private errorHandlerService: ErrorHandlerService,
                private router: Router,
                private formBuilder: FormBuilder) {
    }

    createLoginForm() {
        this.loginForm = this.formBuilder.group({
            email: ['', [Validators.required, Validators.email]],
            password: ['', Validators.required]
        })
    }

    createRegistrationForm() {
        this.registrationForm = this.formBuilder.group({
            email: ['', [Validators.required, Validators.email]],
            password: ['', Validators.required],
            confirm_password: [
                '', Validators.required
            ],
            company_name: '',
            first_name: ['', Validators.required],
            last_name: ['', Validators.required]
        }, {
            validator: confirmPasswordValidator
        })
    }

    ngOnInit() {
        this.createLoginForm();
        this.createRegistrationForm();
    }

    onSubmitLogin() {
        const data = this.loginForm.value;
        data.type = 'login';

        if (this.jwtService.getJwt() !== null) {
            // Include JWT in the request
            data.jwt = this.jwtService.getJwt();
        }

        this.authService.login(data).subscribe(
            response => {
                // save to token in localstorage
                if (response.jwt !== undefined) {
                    this.jwtService.setJwt(response.jwt);
                }

                this.jwtService.setAccessToken(response.access_token);
                this.jwtService.setCustomerName(response.customer.first_name);

                if (response.jwt !== undefined) {
                    this.jwtService.setJwt(response.jwt);
                    return this.router.navigate(['shipping']);
                }

                return this.router.navigate(['/']);

            }, (err: HttpErrorResponse) => {
                this.errorHandlerService.setErrorContainer(err);
            }
        )
    }

    onSubmitRegistration() {
        const data = this.registrationForm.value;

        // set type
        data.type = 'registration';

        this.customerService.register(data).subscribe(
            response => {

                this.jwtService.setAccessToken(response.access_token);
                this.jwtService.setCustomerName(response.customer.first_name);

                if (response.jwt !== undefined) {
                    this.jwtService.setJwt(response.jwt);
                    return this.router.navigate(['shipping']);
                }

                return this.router.navigate(['/']);

            }, (err: HttpErrorResponse) => {
                // Set data for ErrorHandler
                this.errorHandlerService.setErrorContainer(err);
            }
        );
    }
}
