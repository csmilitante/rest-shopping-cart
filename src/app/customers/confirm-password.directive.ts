import { AbstractControl } from '@angular/forms';

export function confirmPasswordValidator(AC: AbstractControl) {
    let password = AC.get('password').value; // to get value in input tag
    let confirmPassword = AC.get('confirm_password').value; // to get value in input tag
    if (password != confirmPassword) {
        AC.get('confirm_password').setErrors({mismatch: true})
    } else {
        return null
    }
}
