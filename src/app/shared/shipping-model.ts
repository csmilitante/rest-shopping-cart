export class Shipping {
    method: string;
    shipping_rate: number;

    constructor(data) {
        this.method = data.method;
        this.shipping_rate = data.shipping_rate;
    }
}
