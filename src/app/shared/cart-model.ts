export class Cart {
    cart_id: number;
    company_name: string;
    customer_id: number;
    discount: number;
    email: string;
    first_name: string;
    last_name: string;
    order_datetime: string;
    phone: string;
    shipping_address1: string;
    shipping_address2: string;
    shipping_address3: string;
    shipping_city: string;
    shipping_country: string;
    shipping_mehod: string;
    shipping_name: string;
    shipping_state: string;
    shipping_total: number;
    sub_total: number;
    tax: number;
    taxable_amount: number;
    total_amount: number;
    total_weight: number;

    constructor(data) {
        this.cart_id = data.cart_id;
        this.company_name = data.company_name;
        this.customer_id = data.customer_id;
        this.discount = data.discount;
        this.email = data.email;
        this.first_name = data.first_name;
        this.last_name = data.last_name;
        this.order_datetime = data.order_datetime;
        this.phone = data.phone;
        this.shipping_address1 = data.shipping_address1;
        this.shipping_address2 = data.shipping_address2;
        this.shipping_address3 = data.shipping_address3;
        this.shipping_city = data.shipping_city;
        this.shipping_country = data.shipping_country;
        this.shipping_mehod = data.shipping_mehod;
        this.shipping_name = data.shipping_name;
        this.shipping_state = data.shipping_state;
        this.shipping_total = data.shipping_total;
        this.sub_total = data.sub_total;
        this.tax = data.tax;
        this.taxable_amount = data.taxable_amount;
        this.total_amount = data.total_amount;
        this.total_weight = data.total_weight;
    }
}
