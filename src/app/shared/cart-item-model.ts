import { Product } from './product-model';

export class CartItem {
    cart_id: number;
    cart_item_id: number;
    price: number;
    product_id: number;
    qty: number;
    unit_price: number;
    weight: number;
    product_details: Product;


    constructor(data) {
        this.cart_id = data.cart_id;
        this.cart_item_id = data.cart_item_id;
        this.price = data.price;
        this.product_id = data.product_id;
        this.qty = parseInt(data.qty);
        this.unit_price = data.unit_price;
        this.weight = data.weight;
        this.product_details = new Product(data.product_details);
    }
}
