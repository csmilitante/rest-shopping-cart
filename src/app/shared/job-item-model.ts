import { Product } from './product-model';

export class JobItem {
    job_item_id: number;
    job_order_id: number;
    product_id: number;
    weight: number;
    qty: number;
    unit_price: number;
    price: number;
    product_details: Product;

    constructor(data) {
        this.job_item_id = data.job_item_id;
        this.job_order_id = data.job_order_id;
        this.product_id = data.product_id;
        this.weight = data.weight;
        this.qty = data.qty;
        this.unit_price = data.unit_price;
        this.price = data.price;
        this.product_details = new Product(data.product_details);
    }
}
