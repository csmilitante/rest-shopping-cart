export class Product {
    price: number;
    product_desc: string;
    product_id: number;
    product_image: string;
    product_name: string;
    product_thumbnail: string;
    stock_qty: number;
    taxable_flag: string;
    weight: number;

    constructor(data) {
        this.price = data.price;
        this.product_desc = data.product_desc;
        this.product_id = data.product_id;
        this.product_image = data.product_image;
        this.product_name = data.product_name;
        this.product_thumbnail = data.product_thumbnail;
        this.stock_qty = isNaN(parseInt(data.stock_qty)) ? 0 : parseInt(data.stock_qty);
        this.taxable_flag = data.taxable_flag;
        this.weight = data.weight;
    }

    hasStock() {
        return this.stock_qty <= 0;
    }
}
