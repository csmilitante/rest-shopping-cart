import { Component, OnInit } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { JwtService } from '../../../core/services/jwt.service';
import { Router } from '@angular/router';
import { OrdersService } from '../../services/orders.service';
import { ErrorHandlerService } from '../../../core/services/error-handler.service';
import { JobOrder } from '../../../shared/job-order-model';
import { JobItem } from '../../../shared/job-item-model';

@Component({
    selector: 'app-confirmation',
    templateUrl: './confirmation.component.html',
    styleUrls: ['./confirmation.component.less']
})
export class ConfirmationComponent implements OnInit {
    jobOrders: JobOrder;
    jobItems: JobItem[];

    constructor(private ordersService: OrdersService,
                private jwtService: JwtService,
                private errorHandlerService: ErrorHandlerService,
                private router: Router) {
    }

    ngOnInit() {
        this.ordersService.getJobOrdersAndItems(this.jwtService.getJwt()).subscribe(
            response => {
                this.jobOrders = new JobOrder(response.job_orders);
                this.jobItems = response.job_items.map(item => new JobItem(item));
            }, (err: HttpErrorResponse) => {

                // delete data.jwt
                this.jwtService.invalidateJwt();

                // Set data for ErrorHandler
                this.errorHandlerService.setErrorContainer(err);

                // Redirect to home
                this.router.navigate(['/']);
            }
        );
    }

    onConfirmOrders() {
        const request = {'jwt': this.jwtService.getJwt()};

        this.ordersService.deleteCartAndCartItems(request).subscribe(
            response => {
                this.jwtService.invalidateJwt();
                this.router.navigate(['success']);
            }, (err: HttpErrorResponse) => {
                // Set data for ErrorHandler
                this.errorHandlerService.setErrorContainer(err);
            }
        )
    }
}
