import { Component, OnInit } from '@angular/core';
import { CartsService } from '../../../carts/services/carts.service';
import { JwtService } from '../../../core/services/jwt.service';
import { Router } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http';
import { OrdersService } from '../../services/orders.service';
import { ErrorHandlerService } from '../../../core/services/error-handler.service';
import { Cart } from '../../../shared/cart-model';
import { CartItem } from '../../../shared/cart-item-model';

@Component({
    selector: 'app-payment',
    templateUrl: './payment.component.html',
    styleUrls: ['./payment.component.less']
})
export class PaymentComponent implements OnInit {
    cartItems: CartItem[];
    cartDetails: Cart;

    constructor(private cartsService: CartsService,
                private ordersService: OrdersService,
                private errorHandlerService: ErrorHandlerService,
                private jwtService: JwtService,
                private router: Router) {
    }

    ngOnInit() {
        this.cartsService.getCartItems(this.jwtService.getJwt()).subscribe(
            response => {
                this.cartItems = response.carts.cart_items.map(item => new CartItem(item));
                this.cartDetails = new Cart(response.carts.cart);
            }, (err: HttpErrorResponse) => {

                // delete data.jwt
                this.jwtService.invalidateJwt();

                this.errorHandlerService.setErrorContainer(err);

                // Redirect to home
                this.router.navigate(['/']);
            }
        );
    }

    onProcessPayment() {
        const data = {'jwt': this.jwtService.getJwt()};
        this.ordersService.transferCartsToJobOrders(data).subscribe(
            response => {
                this.jwtService.setJwt(response.jwt);
                this.router.navigate(['orders/confirm']);
            }, (err: HttpErrorResponse) => {

                // delete data.jwt
                this.jwtService.invalidateJwt();

                this.errorHandlerService.setErrorContainer(err);

                // Redirect to home
                this.router.navigate(['/']);
            }
        );
    }
}
