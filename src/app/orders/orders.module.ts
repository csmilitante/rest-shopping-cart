import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { OrdersRoutingModule } from './orders-routing.module';
import { PaymentComponent } from './components/payment/payment.component';
import { ConfirmationComponent } from './components/confirmation/confirmation.component';
import { OrdersService } from './services/orders.service';
import { CartsService } from '../carts/services/carts.service';

@NgModule({
    imports: [
        CommonModule,
        OrdersRoutingModule
    ],
    declarations: [
        PaymentComponent,
        ConfirmationComponent
    ],
    providers: [
        CartsService,
        OrdersService
    ]
})
export class OrdersModule {
}
