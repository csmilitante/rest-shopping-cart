import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../config/environment';

@Injectable()
export class OrdersService {
    constructor(private http: HttpClient) {
    }

    transferCartsToJobOrders(data: Object): Observable<any> {
        return this.http.post(`${environment.api.uri}/${environment.api.endpoint.orders}`, data);
    }

    getJobOrdersAndItems(data: Object): Observable<any> {
        return this.http.get(`${environment.api.uri}/${environment.api.endpoint.orders}/${data}`);
    }

    deleteCartAndCartItems(data: Object): Observable<any> {
        return this.http.request(
            'delete',
            `${environment.api.uri}/${environment.api.endpoint.orders}`,
            {
                body: data
            }
        );
    }
}
