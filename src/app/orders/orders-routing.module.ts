import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PaymentComponent } from './components/payment/payment.component';
import { ConfirmationComponent } from './components/confirmation/confirmation.component';
import { AuthGuardService } from '../core/guards/auth-guard.service';

const routes: Routes = [
    {
        path: '',
        redirectTo: '/cart',
        pathMatch: 'full'
    },
    {
        path: 'payment',
        component: PaymentComponent,
        canActivate: [AuthGuardService],
    },
    {
        path: 'confirm',
        component: ConfirmationComponent,
        canActivate: [AuthGuardService],
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class OrdersRoutingModule {
}
