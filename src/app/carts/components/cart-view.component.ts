import { Component, DoCheck, OnInit } from '@angular/core';
import { CartsService } from '../services/carts.service';
import { JwtService } from '../../core/services/jwt.service';
import { Router } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http';
import { ErrorHandlerService } from '../../core/services/error-handler.service';
import { Cart } from '../../shared/cart-model';
import { CartItem } from '../../shared/cart-item-model';

@Component({
    selector: 'app-cart-view',
    templateUrl: './cart-view.component.html',
    styleUrls: ['./cart-view.component.less']
})
export class CartViewComponent implements OnInit, DoCheck {
    cartItems: CartItem[];
    cartDetails: Cart;

    constructor(private cartsService: CartsService,
                private jwtService: JwtService,
                private errorHandlerService: ErrorHandlerService,
                private router: Router) {
    }

    ngOnInit() {
        this.cartsService.getCartItems(this.jwtService.getJwt()).subscribe(
            response => {
                this.cartItems = response.carts.cart_items.map(cartItem => new CartItem(cartItem));
                this.cartDetails = new Cart(response.carts.cart);
            }, (err: HttpErrorResponse) => {

                // delete data.jwt
                this.jwtService.invalidateJwt();

                // Set data for ErrorHandler
                this.errorHandlerService.setErrorContainer(err);

                // Redirect to home
                this.router.navigate(['/']);
            }
        );
    }

    ngDoCheck() {
        if (this.cartItems !== undefined) {
            if (this.cartItems.length <= 0) {
                this.jwtService.invalidateJwt();
                this.router.navigate(['/']);
            }
        }
    }

    onDelete(index: any, productId: string) {

        if (window.confirm('Are you sure you want to delete?')) {
            const payload = {
                'product_id': productId,
                'jwt': this.jwtService.getJwt()
            };

            this.cartsService.deleteCartItem(payload).subscribe(
                response => {
                    this.jwtService.setJwt(response.jwt);

                    const removedItem = this.cartItems.splice(index, 1);

                    this.cartDetails.sub_total = this.cartDetails.sub_total - removedItem[0].price;
                    this.cartDetails.total_amount = this.cartDetails.total_amount - removedItem[0].price;
                }
            );
        }
    }
}
