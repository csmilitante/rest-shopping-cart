import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CartViewComponent } from './components/cart-view.component';
import { CartsService } from './services/carts.service';
import { CartsGuardService } from './carts-guard.service';
import { CartsRoutingModule } from './carts-routing.module';

@NgModule({
    imports: [
        CommonModule,
        CartsRoutingModule
    ],
    declarations: [CartViewComponent],
    providers: [
        CartsService,
        CartsGuardService
    ],
})
export class CartsModule {
}
