import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../config/environment';

@Injectable()
export class CartsService {

    constructor(private http: HttpClient) {
    }

    getCartItems(jwt: string): Observable<any> {
        return this.http.get(`${environment.api.uri}/${environment.api.endpoint.carts}/${jwt}`);
    }

    deleteCartItem(data: Object): Observable<any> {
        return this.http.request(
            'delete',
            `${environment.api.uri}/${environment.api.endpoint.cart_items}`,
            {
                body: data
            }
        );
    }

    addToCart(data): Observable<any> {
        return this.http.post(`${environment.api.uri}/${environment.api.endpoint.carts}`, data);
    }
}
