import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CartViewComponent } from './components/cart-view.component';
import { CartsGuardService } from './carts-guard.service';

const cartRoutes: Routes = [
    {
        path: '',
        component: CartViewComponent,
        canActivate: [CartsGuardService],
    }
];

@NgModule({
    imports: [RouterModule.forChild(cartRoutes)],
    exports: [RouterModule]
})
export class CartsRoutingModule {
}
