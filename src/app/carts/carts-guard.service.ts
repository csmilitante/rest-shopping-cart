import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { JwtService } from '../core/services/jwt.service';

@Injectable()
export class CartsGuardService implements CanActivate {
    constructor(private jwtService: JwtService,
                private router: Router) {
    }

    canActivate() {
        if (this.jwtService.getJwt() === null) {
            this.router.navigate(['/']);
            return false;
        }

        return true;
    }
}
