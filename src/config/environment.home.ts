export const environment = {
  production: false,
  api: {
    uri: "http://localhost:8181",
    endpoint: {
      products: "api/products",
      carts: "api/carts",
      shipping: "api/shipping",
      'cart_items': "api/cart-items",
      customers: "api/customers",
      orders: "api/orders",
    }
  }
}
