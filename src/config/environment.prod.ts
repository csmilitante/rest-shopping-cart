export const environment = {
  production: true,
  api: {
    uri: "http://api.exam.com",
    endpoint: {
      products: 'api/products',
      carts: 'api/carts',
      shipping: 'api/shipping',
      'cart_items': 'api/cart-items',
      customers: 'api/customers',
      orders: 'api/orders',
    }
  }
};
